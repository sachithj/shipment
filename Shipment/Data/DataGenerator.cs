﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shipment.Data
{
    using Models;
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ShipmentContext(
                serviceProvider.GetRequiredService<DbContextOptions<ShipmentContext>>()))
            {
                // Look for any board games.
                if (context.Shipments.Any())
                {
                    return;   // Data was already seeded
                }

                context.Shipments.AddRange(
                    new Shipment()
                    {
                        Details = "first shipment details",
                        Label = new Label()
                        {
                            LabelData = "sample Label data"
                        },
                        SupplierOrderNumber = "03984dsof"
                    },
                    new Shipment()
                    {
                        Details = "second shipment details",
                        Label = new Label()
                        {
                            LabelData = "second sample Label data"
                        },
                        SupplierOrderNumber = "sdfd03tryt"
                    });

                context.SaveChanges();
            }
        }
    }
}

