﻿using Microsoft.EntityFrameworkCore;

namespace shipment.Data
{
    using Models;
    public class ShipmentContext : DbContext
    {
        public ShipmentContext(DbContextOptions<ShipmentContext> options)
            : base(options)
        {
        }

        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<Label> Labels { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Label>().HasData(new Label()
            {
                Id = 1,
                LabelData = "sample Label data from context"
            });

            builder.Entity<Shipment>().HasData(new Shipment()
            {
                Id=1,
                Details = "first shipment details from context",
                LabelId = 1,
                SupplierOrderNumber = "03984dsof"
            });
        }
    }
}