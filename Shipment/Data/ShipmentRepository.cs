﻿namespace shipment.Data
{
    using Microsoft.EntityFrameworkCore;
    using Models;
    using System.Collections.Generic;
    using System.Linq;
    using Interfaces;
    public class ShipmentRepository : IShipmentRepository
    {
        private readonly ShipmentContext context;
        private readonly DbSet<Shipment> shipmentEntity;
        public ShipmentRepository(ShipmentContext context)
        {
            this.context = context;
            shipmentEntity = context.Set<Shipment>();
        }


        public void SaveShipment(Shipment shipment)
        {
            context.Entry(shipment).State = EntityState.Added;
            context.SaveChanges();
        }

        public IEnumerable<Shipment> GetAllShipments()
        {
            return shipmentEntity.AsEnumerable();
        }

        public Shipment GetShipment(long id)
        {
            return shipmentEntity.SingleOrDefault(s => s.Id == id);
        }

        public void DeleteShipment(long id)
        {
            Shipment Shipment = GetShipment(id);
            shipmentEntity.Remove(Shipment);
            context.SaveChanges();
        }

        public void UpdateShipment(Shipment shipment)
        {
            context.Entry(shipment).State = EntityState.Modified;
            context.SaveChanges();
        }

    }
}