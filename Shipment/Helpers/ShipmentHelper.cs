﻿using System.Collections.Generic;
using shipment.Interfaces;

namespace shipment.Helpers
{
    using Models;
    public class ShipmentHelper
    {
        public ShipmentHelper(IShipmentRepository shipmentRepository)
        {
            this.ShipmentRepository = shipmentRepository;
        }

        public IShipmentRepository ShipmentRepository { get; set; }

        public IEnumerable<Shipment> GetAllShipments()
        {
            return ShipmentRepository.GetAllShipments();
        }
    }
}
