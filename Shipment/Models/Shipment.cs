﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shipment.Models
{
    public class Shipment
    {
        public int Id { get; set; }
        public string SupplierOrderNumber { get; set; }
        public string TrackingURL { get; set; }
        public string Details { get; set; }
        public Label Label { get; set; }
        public int LabelId { get; set; }
    }
}
