﻿namespace shipment.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using Models;
    using Interfaces;

    [Route("api/[controller]")]
    [ApiController]
    public class ShipmentsController : ControllerBase
    {
        public IShipmentRepository ShipmentRepository { get; }

        public ShipmentsController(IShipmentRepository shipmentRepository)
        {
            this.ShipmentRepository = shipmentRepository;
        }

        [HttpGet]
        public IEnumerable<Shipment> GetByContext()
        {
            return ShipmentRepository.GetAllShipments();
        }

        // GET: api/Shipments/5
        //[HttpGet("{id}", Name = "Get")]
        //public Shipment Get(int id)
        //{
        //    var shipment = new Shipment()
        //    {
        //        Details= "first shipment details",
        //        Label = new Label()
        //        {
        //            LabelData = "sample Label data"
        //        },
        //        SupplierOrderNumber = "03984dsof"
        //    };
        //    return shipment;
        //}

        // POST: api/Shipments
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Shipments/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
