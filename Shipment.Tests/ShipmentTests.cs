using shipment.Controllers;
using shipment.Data;
using shipment.Helpers;
using Xunit;

namespace shipment.Tests
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Models;

    public class ShipmentTests : IClassFixture<DbFixture>
    {
        private readonly ServiceProvider _serviceProvider;

        public ShipmentTests(DbFixture fixture)
        {
            _serviceProvider = fixture.ServiceProvider;
        }

        [Fact]
        public void GetAllShipments_CheckDataAvailability_ShipmentDetails()
        {
            // creating mock context for unit test
            using (var mockedContext = new ShipmentContext(
                _serviceProvider.GetRequiredService<DbContextOptions<ShipmentContext>>()))
            {

                mockedContext.Shipments.AddRange(
                    new Shipment()
                    {
                        Details = "mocked shipment details",
                        Label = new Label()
                        {
                            LabelData = "mocked Label data"
                        },
                        SupplierOrderNumber = "03984122"
                    },
                    new Shipment()
                    {
                        Details = "second shipment details",
                        Label = new Label()
                        {
                            LabelData = "second sample Label data"
                        },
                        SupplierOrderNumber = "3242323"
                    });

                mockedContext.SaveChanges();

                var shipmentRepository = new ShipmentRepository(mockedContext);

                var shipmentHelper = new ShipmentHelper(shipmentRepository);
                var shipments = shipmentHelper.GetAllShipments();

                var e = new Shipment();
                using (var enumerate = shipments.GetEnumerator())
                {
                    if (enumerate.MoveNext())
                        e = enumerate.Current;
                }

                Assert.Equal("mocked shipment details", e.Details);
            }
        }

        [Fact]
        public void NotWorkingShipmentTest()
        {
            using (var context = _serviceProvider.GetService<ShipmentContext>())
            {

                var shipmentRepository = new ShipmentRepository(context);
                var shipmentController = new ShipmentsController(shipmentRepository);
                var shipments = shipmentController.GetByContext();

                var e = new Shipment();
                using (var enumerate = shipments.GetEnumerator())
                {
                    if (enumerate.MoveNext())
                        e = enumerate.Current;
                }
                Assert.Equal("first shipment details from context", e.Details);
            }
        }


        [Fact]
        public void WorkingShipmentTest()
        {
            using (var context = new ShipmentContext(
                _serviceProvider.GetRequiredService<DbContextOptions<ShipmentContext>>()))
            {

                context.Shipments.AddRange(
                    new Shipment()
                    {
                        Details = "first shipment details",
                        Label = new Label()
                        {
                            LabelData = "sample Label data"
                        },
                        SupplierOrderNumber = "03984dsof"
                    },
                    new Shipment()
                    {
                        Details = "second shipment details",
                        Label = new Label()
                        {
                            LabelData = "second sample Label data"
                        },
                        SupplierOrderNumber = "sdfd03tryt"
                    });

                context.SaveChanges();

                var shipmentRepository = new ShipmentRepository(context);
                var shipmentController = new ShipmentsController(shipmentRepository);
                var shipments = shipmentController.GetByContext();

                var e = new Shipment();
                using (var enumerate = shipments.GetEnumerator())
                {
                    if (enumerate.MoveNext())
                        e = enumerate.Current;
                }
                Assert.Equal("first shipment details", e.Details);
            }
        }
    }
}
