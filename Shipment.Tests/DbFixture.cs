﻿using shipment.Data;

namespace shipment.Tests
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public class DbFixture
    {
        public DbFixture()
        {
            var serviceCollection = new ServiceCollection();
            //serviceCollection
                //.AddDbContext<ShipmentContext>(ServiceLifetime.Transient);
            serviceCollection.AddDbContext<ShipmentContext>(options => options.UseInMemoryDatabase(databaseName: "ShipmentsDb"));
            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        public ServiceProvider ServiceProvider { get; private set; }
    }
}
