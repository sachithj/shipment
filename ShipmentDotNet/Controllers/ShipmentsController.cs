﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using ShipmentDotNet.Areas.HelpPage.Models;
using ShipmentDotNet.Data;
using ShipmentDotNet.Interfaces;

namespace ShipmentDotNet.Controllers
{
    public class ShipmentsController : ApiController
    {
        private readonly ShipmentContext _db = new ShipmentContext();

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();


        // GET: api/Shipments
        public IQueryable<Shipment> GetShipments()
        {
            Logger.Info($"Contents are about to display - {DateTime.Now:HH:mm:ss tt zz}");
            //NLog.LogManager.Shutdown();
            return _db.Shipments;
        }

        // GET: api/Shipments/5
        [ResponseType(typeof(Shipment))]
        public IHttpActionResult GetShipment(int id)
        {
            var shipment = _db.Shipments.Find(id);
            if (shipment == null)
            {
                return NotFound();
            }

            return Ok(shipment);
        }

        // PUT: api/Shipments/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutShipment(int id, Shipment shipment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shipment.Id)
            {
                return BadRequest();
            }

            _db.Entry(shipment).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShipmentExists(id))
                {
                    return NotFound();
                }

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Shipments
        [ResponseType(typeof(Shipment))]
        public IHttpActionResult PostShipment(Shipment shipment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.Shipments.Add(shipment);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = shipment.Id }, shipment);
        }

        // DELETE: api/Shipments/5
        [ResponseType(typeof(Shipment))]
        public IHttpActionResult DeleteShipment(int id)
        {
            var shipment = _db.Shipments.Find(id);
            if (shipment == null)
            {
                return NotFound();
            }

            _db.Shipments.Remove(shipment);
            _db.SaveChanges();

            return Ok(shipment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShipmentExists(int id)
        {
            return _db.Shipments.Count(e => e.Id == id) > 0;
        }
    }
}