﻿namespace ShipmentDotNet.Areas.HelpPage.Models
{
    public class Label
    {
        public string LabelData { get; set; }
        public int Id { get; set; }
    }
}
