﻿using System.Collections.Generic;
using System.Data.Entity;
using ShipmentDotNet.Areas.HelpPage.Models;

namespace ShipmentDotNet.Data
{
    public class ShipmentDbInitializer : DropCreateDatabaseAlways<ShipmentContext>
    {
        protected override void Seed(ShipmentContext context)
        {
            IList<Shipment> defaultStandards = new List<Shipment>();

            defaultStandards.Add(new Shipment()
            {
                Details = "dotnet entity framework shipment details",
                Label = new Label()
                {
                    LabelData = "dotnet entity framework Label data"
                },
                SupplierOrderNumber = "03984122"
            });
            context.Shipments.AddRange(defaultStandards);

            base.Seed(context);
        }
    }
}