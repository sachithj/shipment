﻿using System.Data.Entity;
using ShipmentDotNet.Areas.HelpPage.Models;

namespace ShipmentDotNet.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using Interfaces;
    public class ShipmentRepository : IShipmentRepository
    {
        private readonly ShipmentContext _context;
        private readonly DbSet<Shipment> _shipmentEntity;
        public ShipmentRepository(ShipmentContext context)
        {
            this._context = context;
            _shipmentEntity = context.Set<Shipment>();
        }


        public void SaveShipment(Shipment shipment)
        {
            _context.Entry(shipment).State = EntityState.Added;
            _context.SaveChanges();
        }

        public IEnumerable<Shipment> GetAllShipments()
        {
            return _shipmentEntity.AsEnumerable();
        }

        public Shipment GetShipment(long id)
        {
            return _shipmentEntity.SingleOrDefault(s => s.Id == id);
        }

        public void DeleteShipment(long id)
        {
            var Shipment = GetShipment(id);
            _shipmentEntity.Remove(Shipment);
            _context.SaveChanges();
        }

        public void UpdateShipment(Shipment shipment)
        {
            _context.Entry(shipment).State = EntityState.Modified;
            _context.SaveChanges();
        }

    }
}