﻿using System.Data.Entity;
using ShipmentDotNet.Areas.HelpPage.Models;

namespace ShipmentDotNet.Data
{
    public class ShipmentContext : DbContext
    {
        public ShipmentContext():base("ShipmentDB") 
        {
            Database.SetInitializer(new ShipmentDbInitializer());
        }

        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<Label> Labels { get; set; }

    }
}