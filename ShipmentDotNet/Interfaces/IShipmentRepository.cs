﻿using ShipmentDotNet.Areas.HelpPage.Models;

namespace ShipmentDotNet.Interfaces
{
    using System.Collections.Generic;
    public interface IShipmentRepository
    {
        void SaveShipment(Shipment shipment);
        IEnumerable<Shipment> GetAllShipments();
        Shipment GetShipment(long id);
        void DeleteShipment(long id);
        void UpdateShipment(Shipment shipment);
    }
}
